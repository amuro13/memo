
從命令列建立新儲存庫。
```
touch README.md
git init
git checkout -b main
git add README.md
git commit -m "first commit"
git remote add origin https://gitea.com/amuro13/memo.git
git push -u origin main
```

從命令列推送已存在的儲存庫
```
git remote add origin https://gitea.com/amuro13/memo.git
git push -u origin main
```
