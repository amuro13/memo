#!/bin/bash
wgPath="/etc/wireguard"
wgConfigServer="${wgPath}/wg0.conf"
wgPrefix="192.168.100"
wgServer="172.16.10.10"
wgServerPort="51820"
wgOutInterface="public"
wgDNS="172.16.10.11"
wgcAmount=3

function wgKey(){
  ## create privateKey and publicKey of server
  wg genkey | tee server_privateKey | wg pubkey > server_publicKey

  ## create clientKey
  for wg in $( seq 1 ${wgcAmount} ); do
    wg genkey | tee client_privateKey_${wg} | wg pubkey > client_publicKey_${wg}
  done

  ## option: create PresharedKey for enhance security
  #wg genpsk > psk
}

function wgInterface() {
  cat << EOF > ${wgConfigServer}
[Interface]
Address = ${wgPrefix}.254/24
PrivateKey = $(cat server_privateKey)
MTU = 1500
ListenPort = 51820
PostUp = sh /etc/wireguard/wg0_up.sh
PostDown = sh /etc/wireguard/wg0_down.sh

EOF

  cat << EOF > ${wgPath}/wg0_up.sh
#!/bin/sh
iptables -F
iptables -F -t nat
iptables -A FORWARD -i wg0 -j ACCEPT
iptables -A FORWARD -o wg0 -j ACCEPT
iptables -t nat -A POSTROUTING -o ${wgOutInterface} -j MASQUERADE
EOF

  cat << EOF > ${wgPath}/wg0_down.sh
#!/bin/sh
iptables -D FORWARD -i wg0 -j ACCEPT
iptables -D FORWARD -o wg0 -j ACCEPT
iptables -t nat -D POSTROUTING -o ${wgOutInterface} -j MASQUERADE
EOF

chmod +x ${wgPath}/*.sh
}

function wgPeer() {
  for wg in $( seq 1 ${wgcAmount} ); do
  cat << EOF >> ${wgConfigServer}
[Peer]
AllowedIPs = ${wgPrefix}.${wg}/32
PublicKey = $(cat client_publicKey_${wg})

EOF
  done
}

function wgc() {
  for wg in $( seq 1 ${wgcAmount} ); do
    cat << EOF > ${wgPath}/wgc${wg}.conf
[Interface]
PrivateKey = $(cat client_privateKey_${wg})
Address = ${wgPrefix}.${wg}/24
DNS = ${wgDNS}

[Peer]
PublicKey = $(cat server_publicKey)
AllowedIPs = 0.0.0.0/0
Endpoint = ${wgServer}:${wgServerPort}
PersistentKeepalive = 30
EOF
done
}

function wgOpenRc() {
  wget https://gitea.com/amuro13/memo/raw/branch/main/linux/_service/wireguard/openrc.conf -O /etc/init.d/wireguard
  chmod +x /etc/init.d/wireguard
  rc-update add wireguard default
  rc-service wireguard start
}

service=$1
case $service in
  wg) wgKey ; wgInterface ; wgPeer ; wgOpenRc ;;
  wgc) wgc ;;
  *) echo "Please input argument: wg or wgc " ;;
esac
